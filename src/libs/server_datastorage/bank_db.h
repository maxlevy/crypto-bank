#ifndef __SERVER_DATASTORAGE_BANK_DB_H_
#define __SERVER_DATASTORAGE_BANK_DB_H_

#include "kj/async.h"
#include "database.h"
#include "server_datastorage.h"

namespace crypto {
  namespace bank {
    namespace server {

      //Simple database
      class db : public datastorage {
      public:

        void open(const std::string & file_name);

        kj::Promise<void> createAccount(
          const api::account_id_t & account_id,
          const api::fund_t initial_balance) override;

        kj::Promise<void> delete_account(
          const api::account_id_t & account_id) override;

        kj::Promise<void> transfer_funds(
          const api::account_id_t & sourceAccount,
          const api::account_id_t & destinationAccount,
          const api::fund_t fundsAmount) override;


      private:
        orm::database db_;
        static void migrate(orm::database_transaction & t, int db_version);
      };
    }
  }
}


#endif//__SERVER_DATASTORAGE_BANK_DB_H_
