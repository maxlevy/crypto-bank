#include "bank_db.h"
#include "database_orm.h"

namespace crypto {
  namespace bank {
    namespace orm {
      class user_account : public database_table {
      public:
        user_account()
          : database_table("user_account"),
          id(this, "id"),
          fund(this, "fund") {

        }

        database_column<std::string> id;
        database_column<int64_t> fund;
      };
    }
  }
}

void crypto::bank::server::db::open(const std::string& file_name) {
  db_.open(file_name);

  int64_t db_version;
  orm::database_transaction t(db_);

  try {
    auto st = t.parse("SELECT version FROM module WHERE id='kernel'");
    if (!st.execute()) {
      throw std::runtime_error("Database has been corrupted");
    }

    st.get_value(0, db_version);
  }
  catch(const std::runtime_error &) {
    db_version = 0;
  }
  migrate(t, db_version);
}

kj::Promise<void> crypto::bank::server::db::createAccount(
  const api::account_id_t & account_id,
  const api::fund_t initial_balance) {
  
  return db_.begin_transaction([account_id_str = std::to_string(account_id), initial_balance](orm::database_transaction & t) {
    
    orm::user_account t1;

    auto st = t.get_reader(t1.select(t1.id).where(t1.id == account_id_str));
    if(st.execute()) {
      throw std::runtime_error("Account already exists");
    }

    t.execute(t1.insert(t1.id = account_id_str, t1.fund = initial_balance));
  });
}

kj::Promise<void> crypto::bank::server::db::delete_account(const api::account_id_t& account_id) {
  return db_.begin_transaction([account_id_str = std::to_string(account_id)](orm::database_transaction & t) {

    orm::user_account t1;

    auto st = t.get_reader(t1.select(t1.id).where(t1.id == account_id_str));
    if (!st.execute()) {
      throw std::runtime_error("Account is not exists");
    }

    t.execute(t1.delete_if(t1.id == account_id_str));
  });
}

kj::Promise<void> crypto::bank::server::db::transfer_funds(const api::account_id_t& sourceAccount,
  const api::account_id_t& destinationAccount, const api::fund_t fundsAmount) {
  return db_.begin_transaction([
      source_account_id_str = std::to_string(sourceAccount),
      destination_account_id_str = std::to_string(destinationAccount),
      fundsAmount](orm::database_transaction & t) {

    orm::user_account t1;

    auto st = t.get_reader(t1.select(t1.fund).where(t1.id == source_account_id_str));
    if (!st.execute()) {
      throw std::runtime_error("Source account is not exists");
    }

    auto sourceFunds = t1.fund.get(st);
    if(sourceFunds < sourceFunds) {
      throw std::runtime_error("Not enough money");
    }

    st = t.get_reader(t1.select(t1.fund).where(t1.id == destination_account_id_str));
    if (!st.execute()) {
      throw std::runtime_error("Destination account is not exists");
    }
    auto destFunds = t1.fund.get(st);

    //TODO: small ORM limit
    t.execute(t1.update(t1.fund = sourceFunds - fundsAmount).where(t1.id == source_account_id_str));
    t.execute(t1.update(t1.fund = destFunds + fundsAmount).where(t1.id == destination_account_id_str));
  });
}

void crypto::bank::server::db::migrate(orm::database_transaction& t, int db_version) {
  t.execute("BEGIN TRANSACTION");
  if (1 > db_version) {
    
    t.execute("CREATE TABLE module(\
		  id VARCHAR(64) PRIMARY KEY NOT NULL,\
		  version INTEGER NOT NULL,\
		  installed DATETIME NOT NULL)");

    t.execute("CREATE TABLE user_account(\
		  id CHAR(64) PRIMARY KEY NOT NULL,\
		  fund INTEGER NOT NULL)");

    t.execute("INSERT INTO module(id, version, installed) VALUES('kernel', 1, datetime('now'))");

  }
  t.execute("COMMIT TRANSACTION");
}
