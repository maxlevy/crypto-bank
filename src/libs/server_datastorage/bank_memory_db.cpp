#include "bank_memory_db.h"
#include "database_orm.h"

kj::Promise<void> crypto::bank::server::memory_db::createAccount(
  const api::account_id_t & account_id,
  const api::fund_t initial_balance) {
  
  std::lock_guard<std::mutex> lock(data_mutex_);

  if(data_.end() != data_.find(account_id)) {
    throw std::runtime_error("Account already exists");
  }

  data_[account_id] = initial_balance;

  return kj::READY_NOW;
}

kj::Promise<void> crypto::bank::server::memory_db::delete_account(const api::account_id_t& account_id) {
  std::lock_guard<std::mutex> lock(data_mutex_);

  auto p = data_.find(account_id);
  if (data_.end() == p) {
    throw std::runtime_error("Account is not exists");
  }

  data_.erase(p);

  return kj::READY_NOW;
}

kj::Promise<void> crypto::bank::server::memory_db::transfer_funds(const api::account_id_t& sourceAccount,
  const api::account_id_t& destinationAccount, const api::fund_t fundsAmount) {
  std::lock_guard<std::mutex> lock(data_mutex_);

  auto p1 = data_.find(sourceAccount);
  if (data_.end() == p1) {
    throw std::runtime_error("Source account is not exists");
  }

  if(p1->second < fundsAmount) {
    throw std::runtime_error("Not enough money");
  }

  auto p2 = data_.find(destinationAccount);
  if (data_.end() == p2) {
    throw std::runtime_error("Destination account is not exists");
  }

  p1->second -= fundsAmount;
  p2->second += fundsAmount;

  return kj::READY_NOW;
}
