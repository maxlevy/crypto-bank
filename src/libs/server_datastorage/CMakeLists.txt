project(crypto_bank_server_datastorage CXX C)
cmake_minimum_required(VERSION 2.6.2)

find_package(CapnProto)

add_library(crypto_bank_server_datastorage STATIC
	server_datastorage.h
	bank_db.h bank_db.cpp
	bank_memory_db.h bank_memory_db.cpp
)

target_include_directories(crypto_bank_server_datastorage
	PRIVATE
	    ${server_api_INCLUDE_DIR}
		${crypto_bank_orm_sqlite_INCLUDE_DIR}
		${CAPNP_INCLUDE_DIRS}
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}
)

target_link_libraries(crypto_bank_server_datastorage
	crypto_bank_orm_sqlite
	crypto_bank_server_api
)
