#include <cstring>
#include <stdexcept>
#include "server_api.h"

crypto::bank::api::account_id_t::account_id_t(const unsigned char init_value[32]) {
  memcpy(id, init_value, sizeof(id));
}

static inline bool is_hex(const char ch) {
  return (ch >= '0' && ch <= '9')
    || (ch >= 'a' && ch <= 'f')
    || (ch >= 'A' && ch <= 'F');
}

static inline unsigned char xtoi(const char ch) {

  if (ch >= '0' && ch <= '9') {
    return ch - '0';
  }

  if (ch >= 'a' && ch <= 'f') {
    return ch - 'a' + 10;
  }

  if (ch >= 'A' && ch <= 'F') {
    return ch - 'A' + 10;
  }

  throw std::runtime_error("Ivalid argument");
}

crypto::bank::api::account_id_t crypto::bank::api::account_id_t::parse(const std::string & value)
{
  if (value.length() != sizeof(id) * 2) {
    throw std::runtime_error("Ivalid account ID " + value);
  }

  unsigned char init_value[sizeof(id)];
  auto p = value.c_str();
  for (int i = 0; i < sizeof(id); ++i, p += 2) {
    if (!is_hex(*p) || !is_hex(p[1])) {
      throw std::runtime_error("Ivalid account ID " + value);
    }

    init_value[i] = (xtoi(*p) << 4) | xtoi(p[1]);
  }

  return account_id_t(init_value);
}

static inline char itox(const unsigned char ch) {
  return (ch < 10) ? (ch + '0') : (ch - 10 + 'A');
}

std::string std::to_string(const crypto::bank::api::account_id_t & value)
{
  char result[sizeof(value.id) * 2 + 1];
  auto p = result;
  for (int i = 0; i < sizeof(value.id);) {
    *p++ = itox(0x0F & (value.id[i] >> 4));
    *p++ = itox(0x0F & (value.id[i++]));
  }

  *p = '\0';

  return result;
}
