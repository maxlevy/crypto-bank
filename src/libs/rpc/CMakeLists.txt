project(crypto_bank_rpc CXX C)
cmake_minimum_required(VERSION 2.6.2)

find_package(CapnProto)
capnp_generate_cpp(CAPNP_SRCS CAPNP_HDRS bank.capnp)

ADD_LIBRARY(crypto_bank_rpc
	STATIC
		bank.capnp
		bank.capnp.h bank.capnp.c++
		utils.h utils.cpp
)

target_include_directories(crypto_bank_rpc
	PRIVATE
		${crypto_bank_server_api_INCLUDE_DIR}
		${CAPNP_INCLUDE_DIRS}
	PUBLIC
		${CMAKE_CURRENT_BINARY_DIR}
		${CMAKE_CURRENT_SOURCE_DIR}
)
 
target_link_libraries(crypto_bank_rpc
	crypto_bank_server_api
)
