#include "utils.h"

namespace crypto {
  namespace bank {
    namespace utils {

      crypto::bank::api::account_id_t deserialize_account_id(
        ::capnp::List<::uint8_t, ::capnp::Kind::PRIMITIVE>::Reader account_id_param) {

        KJ_IREQUIRE(account_id_param.size() == sizeof(crypto::bank::api::account_id_t::id));

        unsigned char result[sizeof(crypto::bank::api::account_id_t::id)];
        for (int i = 0; i < sizeof(crypto::bank::api::account_id_t::id); ++i) {
          result[i] = account_id_param[i];
        }

        return crypto::bank::api::account_id_t(result);
      }

      kj::Array<::uint8_t> seserialize_account_id(const api::account_id_t& value) {
        auto result = kj::heapArray<::uint8_t>(sizeof(crypto::bank::api::account_id_t::id));
        for (int i = 0; i < sizeof(crypto::bank::api::account_id_t::id); ++i) {
          result[i] = value.id[i];
        }

        return result;
      }
    }
  }
}