#ifndef __CRYPTO_BANK_BANK_IMPL_H__
#define __CRYPTO_BANK_BANK_IMPL_H__

#include "bank.capnp.h"
#include "server_datastorage.h"

namespace crypto {
  namespace bank {
    namespace server {

      class bank_impl : public crypto::bank::rpc::Bank::Server {
      public:

        bank_impl(server::datastorage & db);
        ~bank_impl();


        kj::Promise<void> createAccount(CreateAccountContext context) override;
        kj::Promise<void> deleteAccount(DeleteAccountContext context) override;
        kj::Promise<void> transferFunds(TransferFundsContext context) override;

      private:
        server::datastorage & db_;
      };
    }
  }
}


#endif//__CRYPTO_BANK_BANK_IMPL_H__