#ifndef __CRYPTO_BANK_SERVER_APP_H__
#define __CRYPTO_BANK_SERVER_APP_H__

#include "bank.capnp.h"
#include "server_datastorage.h"

namespace crypto {
  namespace bank {
    namespace server {

      class app {
      public:
        app(const char * server_address, datastorage & db);


        capnp::uint port() {
          return server_.getPort().wait(wait_scope_);
        }

        int run() {
          // Run forever, accepting connections and handling requests.
          kj::NEVER_DONE.wait(wait_scope_);
          return 0;
        }

      private:
        capnp::EzRpcServer server_;
        kj::WaitScope & wait_scope_;
      };
    }
  }
}


#endif//__CRYPTO_BANK_SERVER_APP_H__