project(crypto_bank_client_app CXX C)
cmake_minimum_required(VERSION 2.6.2)

find_package(CapnProto)

add_executable(crypto_bank_client_app
	client.h client.cpp)

target_include_directories(crypto_bank_client_app
	PRIVATE
		${crypto_bank_rpc_INCLUDE_DIR}
		${crypto_bank_client_INCLUDE_DIR}
		${CAPNP_INCLUDE_DIRS}
	PUBLIC
		${CMAKE_CURRENT_SOURCE_DIR}
)

target_link_libraries(crypto_bank_client_app
  crypto_bank_rpc
  crypto_bank_client
  CapnProto::capnp
  CapnProto::capnp-rpc
)
