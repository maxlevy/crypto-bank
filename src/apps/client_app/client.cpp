#include <string>
#include <iostream>
#include <capnp/ez-rpc.h>
#include <kj/debug.h>
#include "client.h"
#include "client_app.h"

static void show_help(const char * program) {
  std::cerr << "usage: " << program << " HOST:PORT Command {Arguments}\n"
    "Connects to the Bank server at the given address and "
    "does some RPCs." << std::endl;
}

int main(int argc, const char* argv[]) {
  if (argc < 2) {
    show_help(argv[0]);
    return 1;
  }

  try {
    crypto::bank::client::app app(argv[1]);

    std::string cmd = argv[2];
    if ("create" == cmd) {
      if (argc != 5) {
        std::cerr << "usage: " << argv[0] << " HOST:PORT create account_id initial_funds\n" << std::endl;
        return 1;
      }

      app.create_account(crypto::bank::api::account_id_t::parse(argv[3]), atoi(argv[4]));
    }
    else if ("delete" == cmd) {
      if (argc != 4) {
        std::cerr << "usage: " << argv[0] << " HOST:PORT delete account_id\n" << std::endl;
        return 1;
      }

      app.delete_account(crypto::bank::api::account_id_t::parse(argv[3]));
    }
    else if ("transfer" == cmd) {
      if (argc != 6) {
        std::cerr << "usage: " << argv[0] << " HOST:PORT transfer account_id target_t funds\n" << std::endl;
        return 1;
      }

      app.transfer_funds(
        crypto::bank::api::account_id_t::parse(argv[3]),
        crypto::bank::api::account_id_t::parse(argv[4]),
        atoi(argv[5]));
    }
    else {
      std::cerr << "Invalid command: " << cmd << std::endl;
      return 2;
    }

    return 0;
  }
  catch (const std::exception & ex) {
    std::cerr << "failed: " << ex.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cerr << "Unexpected error" << std::endl;
    return 1;
  }
}

