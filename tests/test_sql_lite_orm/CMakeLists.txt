cmake_minimum_required(VERSION 2.8)
project(test_sql_lite_orm)

find_package (GTest REQUIRED)
find_package(CapnProto REQUIRED)

if(UNIX)
	find_package (Threads REQUIRED)
endif(UNIX)

enable_testing()

add_executable(test_sql_lite_orm
	sql_builder_tests.cpp
	sql_builder_tests.h
	test_sql_lite_orm.cpp
)

target_include_directories(test_sql_lite_orm
	PRIVATE
		${crypto_bank_orm_sqlite_INCLUDE_DIR}
		${CAPNP_INCLUDE_DIRS}
   		${GTEST_INCLUDE_DIRS}
)

target_link_libraries(test_sql_lite_orm
  crypto_bank_orm_sqlite
  ${GTEST_LIBRARIES}
  ${CMAKE_DL_LIBS}
  ${PTHREAD_LIB}
  ${CMAKE_THREAD_LIBS_INIT}
)

add_test(
    NAME runUnitTests
    COMMAND test_sql_lite_orm
)
