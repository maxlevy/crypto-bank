#include "gtest/gtest.h"
#include "sql_builder_tests.h"

crypto::bank::orm::mock_database::mock_database()
{  
}

crypto::bank::orm::mock_database::~mock_database()
{
}

crypto::bank::orm::mock_sql_statement::mock_sql_statement(mock_sql_statement &&) noexcept
{
}

crypto::bank::orm::mock_sql_statement::mock_sql_statement(sqlite3 * db, const char * sql)
{
}

crypto::bank::orm::mock_sql_statement::~sql_statement()
{
}

bool crypto::bank::orm::mock_sql_statement::execute()
{
  return false;
}

static int int_parameter_value;

void crypto::bank::orm::mock_sql_statement::set_parameter(int , int value)
{
  int_parameter_value = value;
}

static std::string string_parameter_value;

void crypto::bank::orm::mock_sql_statement::set_parameter(int , const std::string & value)
{
  string_parameter_value = value;
}


static std::string result_sql;

crypto::bank::orm::mock_sql_statement crypto::bank::orm::mock_database_read_transaction::parse(const char * sql) const
{
  result_sql = sql;
  return mock_sql_statement(nullptr, sql);
}

static inline void replace_string(std::string & result, const std::string & original, const std::string & target) {
  size_t index = 0;
  for (;;) {
    index = result.find(original, index);
    if (index == std::string::npos) {
      break;
    }
    result.replace(index, original.length(), target);
    index += target.length();
  }
}

TEST(sql_builder_tests, test_select) {

  crypto::bank::orm::database db;
  crypto::bank::orm::database_transaction trans(db);

  test_table1 t1;
  test_table2 t2;

  auto reader = trans.get_reader(
    t1
    .select(crypto::bank::orm::db_max(t1.column1), t1.column2, t2.column1)
    .inner_join(t2, t1.column1 == t2.column1)
    .where(t1.column1 == 10 && t2.column2 == "test")
    .order_by(t1.column1, crypto::bank::orm::db_desc_order(t1.column1)));


  replace_string(result_sql, "?1", "?");
  replace_string(result_sql, "?2", "?");

  ASSERT_EQ(result_sql,
    "SELECT MAX(t0.column1),t0.column2,t1.column1 FROM test_table1 t0 INNER JOIN test_table2 t1 ON t0.column1=t1.column1 WHERE (t0.column1=?) AND (t1.column2=?) ORDER BY t0.column1,t0.column1 DESC");

  ASSERT_EQ(int_parameter_value, 10);
  ASSERT_EQ(string_parameter_value, "test");
}


TEST(sql_builder_tests, test_insert) {

  crypto::bank::orm::database db;
  crypto::bank::orm::database_transaction trans(db);
  
  test_table1 t1;

  trans.execute(
    t1.insert(t1.column1 = 10, t1.column2 = "test"));

  replace_string(result_sql, "?1", "?");
  replace_string(result_sql, "?2", "?");

  ASSERT_EQ(result_sql,
    "INSERT INTO test_table1(column1,column2) VALUES (?,?)");

  ASSERT_EQ(int_parameter_value, 10);
  ASSERT_EQ(string_parameter_value, "test");
}

TEST(sql_builder_tests, test_update) {

  crypto::bank::orm::database db;
  crypto::bank::orm::database_transaction trans(db);

  test_table1 t1;

  trans.execute(
    t1.update(t1.column1 = 10, t1.column2 = "test").where(t1.column1 == 20));

  replace_string(result_sql, "?1", "?");
  replace_string(result_sql, "?2", "?");
  replace_string(result_sql, "?3", "?");

  ASSERT_EQ(result_sql,
    "UPDATE test_table1 SET column1=?,column2=? WHERE column1=?");

  ASSERT_EQ(string_parameter_value, "test");
}

TEST(sql_builder_tests, test_insert_from) {

  crypto::bank::orm::database db;
  crypto::bank::orm::database_transaction trans(db);

  test_table1 t1;
  test_table2 t2;

  trans.execute(
    t1.insert_into(t1.column1, t1.column2)
    .from(t2, crypto::bank::orm::db_max(t2.column1), t2.column1, crypto::bank::orm::db_max(crypto::bank::orm::db_length(t2.column2)))
    .where(t2.column2 == "test"));

  ASSERT_EQ(result_sql,
     "INSERT INTO test_table1(column1,column2) SELECT MAX(t0.column1),t0.column1,MAX(LENGTH(t0.column2)) FROM test_table2 t0 WHERE t0.column2=?1");

  ASSERT_EQ(string_parameter_value, "test");
}


TEST(sql_builder_tests, test_delete) {

  crypto::bank::orm::database db;
  crypto::bank::orm::database_transaction trans(db);

  test_table1 t1;

  trans.execute(
    t1.delete_if(t1.column1 == 10));

  replace_string(result_sql, "?1", "?");

  ASSERT_EQ(result_sql,
    "DELETE FROM test_table1 WHERE test_table1.column1=?");
  ASSERT_EQ(int_parameter_value, 10);
}
